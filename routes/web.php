<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/results', 'ReparationController@get_info')->name('results');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/voitures', 'VoitureController')->except('destroy');
    Route::get('/voitures/delete/{id}', 'VoitureController@destroy')->name('voitures.destroy');


    Route::get('/get_modeles/{id}','MarqueController@get_modeles');


    Route::resource('/clients', 'ClientController')->except('destroy');
    Route::get('/clients/delete/{id}', 'ClientController@destroy')->name('clients.destroy');


    Route::resource('/reparations', 'ReparationController')->except('destroy');
    Route::get('/reparations/delete/{id}', 'ReparationController@destroy')->name('reparations.destroy');


    Route::resource('/interventions', 'InterventionController')->except('destroy');
    Route::get('/interventions/delete/{id}', 'InterventionController@destroy')->name('interventions.destroy');


});
