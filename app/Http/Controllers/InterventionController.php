<?php

namespace App\Http\Controllers;

use App\Intervention;
use App\Voiture;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

class InterventionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $intervention = Intervention::all();

        return view('backend.interventions.index',compact('intervention'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.interventions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'libelle' => 'required',
            'prix' => 'required',
        ]);

        $intervention = new Intervention();

        $intervention->libelle = request('libelle');
        $intervention->prix = request('prix');

        $intervention->save();

        return redirect('/interventions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $intervention = Intervention::find($id);

        return view('backend.interventions.edit',compact('intervention'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $intervention = Intervention::find($id);

        return view('backend.interventions.edit',compact('intervention'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate(request(), [
            'libelle' => 'required',
            'prix' => 'required',
        ]);

        $intervention =  Intervention::find($id);

        $intervention->libelle = request('libelle');
        $intervention->prix = request('prix');

        $intervention->save();

        return redirect('/interventions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Intervention  $intervention
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $intervention = Intervention::find($id)->delete();

        return redirect()->back();
    }
}
