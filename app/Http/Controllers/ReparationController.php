<?php

namespace App\Http\Controllers;

use App\Intervention;
use App\Reparation;
use App\User;
use App\Voiture;
use Facade\FlareClient\Http\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ReparationController extends Controller
{

    public function get_info(Request $request)
    {
        $rep = Reparation::where('random_code',$request->code)->get();

        return view('welcome')->with('rep',$rep);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reparations = Reparation::all();

        return view('backend.reparations.index',compact('reparations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $voitures = Voiture::all();
        $interventions = Intervention::all();
        $clients = User::all();


        return view('backend.reparations.create',compact('voitures','interventions','clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [
            'client_id' => 'required',
            'voiture_id' => 'required',
            'intervention_id' => 'required',
        ]);

        $client_id=$request->client_id;
        $voiture_ids=$request->voiture_id;
        $interventions=$request->intervention_id;

        $selected_interventions=[];
        foreach ($interventions as $int){
            if($int)
                $selected_interventions[]=$int;
        }

        $counter =0 ;
        foreach ($voiture_ids as $v)
        {
            $reparations = new  Reparation();
            $reparations->client_id = $client_id;
            $reparations->voiture_id = $v;
            $reparations->intervention_id = $selected_interventions[$counter];
            $reparations->save();
            $counter++;
        }

        return redirect()->back()->with('success', 'Réparation ajouté avec succés');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reparation  $reparation
     * @return \Illuminate\Http\Response
     */
    public function show(Reparation $reparation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reparation  $reparation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clients = User::all();
        $reparation = Reparation::find($id);
        $voitures = Voiture::all();
        $interventions = Intervention::all();

        return view('backend.reparations.edit',compact('reparation','clients','voitures','interventions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reparation  $reparation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'client_id' => 'required',
            'voiture_id' => 'required',
            'intervention_id' => 'required',
        ]);

        $reparations = Reparation::find($id);
        $reparations->client_id = $request->client_id;
        $reparations->voiture_id = $request->voiture_id;
        $reparations->intervention_id = $request->intervention_id;
        $reparations->date_int = $request->date_int;
        $reparations->status = $request->status;

        $uniqid = uniqid();

        $rand_start = rand(1,5);

        $rand_8_char = substr($uniqid,$rand_start,8);

        $reparations->random_code = $rand_8_char;
        $reparations->save();

        return redirect('/reparations')->with('success', 'Réparation éditer avec succés');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reparation  $reparation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reparations = Reparation::find($id)->delete();

        return redirect()->back()->with('errors', 'Réparation supprimé avec succés');
    }
}
