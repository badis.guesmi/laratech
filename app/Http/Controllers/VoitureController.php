<?php

namespace App\Http\Controllers;

use App\Marque;
use App\Modele;
use App\Voiture;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class VoitureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voitures = Voiture::all();

        return view('backend.voitures.index',compact('voitures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marques = Marque::all();
        $modeles = Modele::all();
        return view('backend.voitures.create',compact('modeles','marques'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'titre' => 'required',
            'marque_id' => 'required',
            'modele_id' => 'required',
            'coleur_int' => 'required',
            'coleur_ext' => 'required',
        ]);

        $voiture = new Voiture();

        $voiture->titre = request('titre');
        $voiture->marque_id = request('marque_id');
        $voiture->modele_id = request('modele_id');
        $voiture->coleur_int = request('coleur_int');
        $voiture->coleur_ext = request('coleur_ext');
        $voiture->save();

        return redirect('/voitures');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voiture  $voiture
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $voiture = Voiture::find($id);
        $marques = Marque::all();
        $modeles = Modele::all();

        return view('backend.voitures.edit',compact('voiture','marques','modeles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voiture  $voiture
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $voiture = Voiture::find($id);
        $marques = Marque::all();
        $modeles = Modele::all();

        return view('backend.voitures.edit',compact('voiture','marques','modeles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voiture  $voiture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'titre' => 'required',
            'marque_id' => 'required',
            'modele_id' => 'required',
            'coleur_int' => 'required',
            'coleur_ext' => 'required',
        ]);

        $voiture = Voiture::find($id);

        $voiture->titre = request('titre');
        $voiture->marque_id = request('marque_id');
        $voiture->modele_id = request('modele_id');
        $voiture->coleur_int = request('coleur_int');
        $voiture->coleur_ext = request('coleur_ext');
        $voiture->save();

        return redirect('/voitures');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voiture  $voiture
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voiture = Voiture::find($id)->delete();

        return redirect()->back();
    }
}
