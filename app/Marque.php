<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marque extends Model
{
    public function voitures(){
        return $this->hasMany('App\Voitures');
    }

    public function modeles(){
        return $this->hasMany('App\Modele');
    }
}
