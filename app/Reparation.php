<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reparation extends Model
{
    public function clients(){
        return $this->belongsTo('App\User','client_id');
    }

    public function interventions(){
        return $this->belongsTo('App\Intervention','intervention_id');
    }

    public function voitures(){
        return $this->belongsTo('App\Voiture','voiture_id');
    }
}
