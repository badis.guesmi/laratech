<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voiture extends Model
{
    public function marques(){
        return $this->belongsTo('App\Marque','marque_id');
    }

    public function modeles(){
        return $this->belongsTo('App\Modele','modele_id');
    }
}
