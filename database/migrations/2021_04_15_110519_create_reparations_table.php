<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReparationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reparations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('voiture_id');
            $table->unsignedBigInteger('intervention_id');
            $table->date('date_int')->nullable();
            $table->date('status')->nullable();


            $table->foreign('client_id', 'client_id_foreign')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('voiture_id', 'voiture_id_id_foreign')
                ->references('id')->on('voitures')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('intervention_id', 'intervention_id_fk')
                ->references('id')->on('interventions')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reparations');
    }
}
