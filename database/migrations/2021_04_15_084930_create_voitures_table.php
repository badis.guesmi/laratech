<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voitures', function (Blueprint $table) {
            $table->id();

            $table->string('titre');
            $table->unsignedBigInteger('marque_id');
            $table->unsignedBigInteger('modele_id');

            $table->string('coleur_ext');
            $table->string('coleur_int');
            $table->foreign('marque_id', 'marque_id_foreign')
                ->references('id')->on('marques')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('modele_id', 'modele_id_foreign')
                ->references('id')->on('modeles')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voitures');
    }
}
