@extends('layouts.dashboard')

@section('content')

    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Ajouter voiture</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post" action="{{route('voitures.store')}}">
                @csrf
                <div class="card-body">
                    @if(count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="exampleInputEmail1">Sélectionner la marque</label>
                        <select class="form-control" name="marque_id" id="my_select">
                            <option>Sélectionner une marque</option>
                        @foreach($marques as $marque)
                            <option value="{{$marque->id}}">{{$marque->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Sélectionner le modéle</label>
                        <select class="form-control" name="modele_id" id="modele_id">

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Titres</label>
                        <input type="text" class="form-control" name="titre" placeholder="titre">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Coleur intérieur</label>
                        <input type="text" class="form-control" name="coleur_int">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Coleur exterieur</label>
                        <input type="text" class="form-control" name="coleur_ext">
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </section>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script>

        $('#my_select').on('change', function (e) {

            var marque_id = e.target.value;
            $.ajax({
                type: 'GET',
                url: "{{url('/get_modeles')}}/" + marque_id,
                success: function (data) {
                    console.log(data);
                    $('#modele_id').empty();
                        $html = '';
                        $.each(data, function (index, codesObj) {
                            $html += '<option value="' + codesObj.id + '" >' + codesObj.title + '</option>';

                        })
                        $('#modele_id').append($html);

                }

            });
        });

    </script>
@endsection
