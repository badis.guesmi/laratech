@extends('layouts.dashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div style="float: right">
                                <a href="{{route('voitures.create')}}" class="btn btn-success">Ajouter voiture</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Marque</th>
                                    <th>Modéle</th>
                                    <th>titre(s)</th>
                                    <th>Coleur intérieur</th>
                                    <th>Coleur extérieur</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($voitures as $v)
                                <tr>
                                    <td>{{$v->marques->title}}</td>
                                    <td>{{$v->modeles->title}}
                                    </td>
                                    <td>{{$v->titre}}</td>
                                    <td>{{$v->coleur_int}}</td>
                                    <td>{{$v->coleur_ext}}</td>

                                    <td>
                                        <a href="{{route('voitures.show',$v->id)}}"class="btn btn-warning">Afficher</a>
                                        <a href="{{route('voitures.edit',$v->id)}}" class="btn btn-info">Editer</a>
                                        <a href="{{route('voitures.destroy',$v->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>

                                    </td>
                                </tr>
                                @endforeach

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </section>

@endsection
