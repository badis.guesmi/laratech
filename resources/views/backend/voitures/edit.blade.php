@extends('layouts.dashboard')

@section('content')

    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Modifier voiture</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post" action="{{route('voitures.update',$voiture->id)}}">
                @csrf
                {{ method_field('PUT') }}
                <div class="card-body">
                    @if(count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="exampleInputEmail1">Sélectionner la marque</label>
                        <select class="form-control" name="marque_id">
                            @foreach($marques as $marque)
                                <option value="{{$marque->id}}" @if($voiture->marque_id == $marque->id) selected @endif>{{$marque->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Sélectionner le modéle</label>
                        <select class="form-control" name="modele_id">
                            @foreach($modeles as $m)
                                <option value="{{$m->id}}" @if($voiture->modele_id == $m->id) selected @endif>{{$m->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Titres</label>
                        <input type="text" class="form-control" name="titre" value="{{$voiture->titre}}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Coleur intérieur</label>
                        <input type="text" class="form-control" name="coleur_int" value="{{$voiture->coleur_int}}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Coleur exterieur</label>
                        <input type="text" class="form-control" name="coleur_ext" value="{{$voiture->coleur_ext}}">
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </section>

@endsection
