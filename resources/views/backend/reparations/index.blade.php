@extends('layouts.dashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div style="float: right">
                                <a href="{{route('reparations.create')}}" class="btn btn-success">Ajouter réparation</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if (\Session::has('errors'))
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{!! \Session::get('errors') !!}</li>
                                    </ul>
                                </div>
                            @endif
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Client</th>
                                    <th>Intervention</th>
                                    <th>Voiture</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reparations as $v)
                                    <tr>
                                        <td>{{$v->clients->name}}</td>
                                        <td>{{$v->interventions->libelle}}
                                        </td>
                                        <td>{{$v->voitures->titre}}</td>
                                        <td>{{$v->date_int}}</td>

                                        <td>
                                            <a href="{{route('reparations.show',$v->id)}}"class="btn btn-warning">Afficher</a>
                                            <a href="{{route('reparations.edit',$v->id)}}" class="btn btn-info">Editer</a>
                                            <a href="{{route('reparations.destroy',$v->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>

                                        </td>
                                    </tr>
                                @endforeach

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </section>

@endsection
