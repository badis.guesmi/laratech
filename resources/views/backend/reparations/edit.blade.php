@extends('layouts.dashboard')

@section('content')

    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Modifier Réparation</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post" action="{{route('reparations.update',$reparation->id)}}">
                @csrf
                {{ method_field('PUT') }}

                <div class="card-body">
                    @if(count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="exampleInputEmail1">Client</label>
                        <select name="client_id" class="form-control">
                            <option>Selectionner client</option>
                            @foreach($clients as $client)
                                <option value="{{$client->id}}"
                                        @if($reparation->client_id == $client->id) selected @endif>{{$client->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Voitures</label>
                        <select name="voiture_id" class="form-control">
                            <option>Selectionner voitures</option>
                            @foreach($voitures as $v)
                                <option value="{{$v->id}}"
                                        @if($reparation->voiture_id == $v->id) selected @endif>{{$v->titre}}</option>
                            @endforeach
                        </select>
                    </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Interventions</label>
                            <select name="intervention_id" class="form-control">
                                <option>Selectionner interventions</option>
                                @foreach($interventions as $i)
                                    <option value="{{$i->id}}"
                                            @if($reparation->intervention_id == $i->id) selected @endif>{{$i->libelle}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date</label>
                            <input class="form-control" type="date" name="date_int" value="{{$reparation->date_int}}">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Status</label>
                            <input class="form-control" type="text" name="status" value="{{$reparation->status}}">
                        </div>

                </div>
                <!-- /.card-body -->




                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </section>

@endsection
