@extends('layouts.dashboard')

@section('content')

    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Modifier Information client</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post" action="{{route('clients.update',$client->id)}}">
                @csrf
                {{ method_field('PUT') }}
                <div class="card-body">
                    @if(count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif


                        <div class="form-group">
                            <label for="exampleInputEmail1">Nom</label>
                            <input type="text" class="form-control" name="name" val="nom">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Cin</label>
                            <input type="text" class="form-control" name="cin">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" name="email">
                        </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </section>

@endsection
