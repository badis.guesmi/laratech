@extends('layouts.dashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div style="float: right">
                                <a href="{{route('clients.create')}}" class="btn btn-success">Ajouter client</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Email</th>
                                    <th>CIN</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $c)
                                    <tr>
                                        <td>{{$c->name}}</td>
                                        <td>{{$c->email}}
                                        </td>
                                        <td>{{$c->cin}}</td>
                                        <td>
                                            <a href="{{route('clients.show',$c->id)}}"class="btn btn-warning">Afficher</a>
                                            <a href="{{route('clients.edit',$c->id)}}" class="btn btn-info">Editer</a>
                                            <a href="{{route('clients.destroy',$c->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>

                                        </td>
                                    </tr>
                                @endforeach

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </section>

@endsection
