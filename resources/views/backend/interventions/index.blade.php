@extends('layouts.dashboard')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div style="float: right">
                                <a href="{{route('interventions.create')}}" class="btn btn-success">Ajouter intervention</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Libellé</th>
                                    <th>Prix</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($intervention as $c)
                                    <tr>
                                        <td>{{$c->libelle}}</td>
                                        <td>{{$c->prix}}
                                        </td>
                                        <td>
                                            <a href="{{route('interventions.show',$c->id)}}"class="btn btn-warning">Afficher</a>
                                            <a href="{{route('interventions.edit',$c->id)}}" class="btn btn-info">Editer</a>
                                            <a href="{{route('interventions.destroy',$c->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> </a>

                                        </td>
                                    </tr>
                                @endforeach

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </section>

@endsection
