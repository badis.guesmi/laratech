@extends('layouts.dashboard')

@section('content')

    <section class="content">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Ajouter intervention</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post" action="{{route('interventions.store')}}">
                @csrf
                <div class="card-body">
                    @if(count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="exampleInputEmail1">Libellé</label>
                        <input type="text" class="form-control" name="libelle" placeholder="Libellé">
                    </div>

                    <div class=" form-group">
                        <label for="exampleInputEmail1">Prix</label>
                        <input type="text" class="form-control" name="prix" placeholder="prix">
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </section>

@endsection
